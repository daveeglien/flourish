﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Flourish.Controllers
{
    public class NotificationsController : Controller
    {
        [Route("notifications")]
        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}