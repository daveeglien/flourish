﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Utilities
{
    public enum HttpRequestType
    {
         HttpPost, 
         HttpGet
    }
}
