﻿using com.google.i18n.phonenumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static com.google.i18n.phonenumbers.Phonenumber;
using static com.google.i18n.phonenumbers.PhoneNumberUtil;

namespace Flourish.Services
{
    public class MobileNumberValidator
    {
        public static string FormatPhoneNumber(string mobileNumber)
        {
            var phoneUtil = getInstance();

            PhoneNumber number = phoneUtil.parse(mobileNumber, "GH");

            char[] charsToTrim = { '+'};
            return phoneUtil.format(number, PhoneNumberFormat.INTERNATIONAL).ToString().Trim(charsToTrim).Replace(" ", string.Empty);
        }

        public static bool IsValidPhoneNumber(string mobileNumber)
        {
            var phoneUtil = getInstance();

            PhoneNumber number;
            try
            {
                number = phoneUtil.parse(mobileNumber, "GH");
            }
            catch (NumberParseException e)
            {
                return false;
            }

            if (!phoneUtil.isValidNumber(number)) return false;

            return true;
        }
    }
}
