﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Flourish.Utilities
{
    public class HttpRequestHelper : IHttpRequestHelper
    {
        private ILogger _logger;

        public HttpRequestHelper(ILogger<HttpRequestHelper> logger)
        {
            _logger = logger;
        }

        public async Task<Tuple<HttpStatusCode, string>> MakePostRequest(HttpClient httpClient,
            string url, string payload, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage httpResponse;
            try {
                _logger.LogDebug($"Making Post Request to {url}, payload: {payload}");

                var request = new HttpRequestMessage(HttpMethod.Post, url);

                if(headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                if (payload != null)
                {
                    httpResponse = await httpClient.SendAsync(request);
                }
                else
                {
                    httpResponse = await httpClient.SendAsync(request);
                }

                using (httpResponse)
                {
                    var responseString = await httpResponse.Content.ReadAsStringAsync();
                    _logger.LogDebug($"Response: {responseString}");
                    return new Tuple<HttpStatusCode, string>(httpResponse.StatusCode, responseString);
                }
            }
            catch(Exception ex)
            {
                _logger.LogDebug($"HttpException: {ex}");
                return null;
            }

        }

        public async Task<Tuple<HttpStatusCode, string>> MakeGetRequest(HttpClient httpClient,
            string url, Dictionary<string, string> headers = null)
        {
            HttpResponseMessage httpResponse;
            try
            {
                _logger.LogDebug($"Making Get Request to {url}");

                var request = new HttpRequestMessage(HttpMethod.Get,
                url);

                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                httpResponse = await httpClient.SendAsync(request);

                using (httpResponse)
                {
                    var responseString = await httpResponse.Content.ReadAsStringAsync();
                    _logger.LogDebug($"Response: {responseString}");
                    return new Tuple<HttpStatusCode, string>(httpResponse.StatusCode, responseString);
                }
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"HttpException: {ex}");
                return null;
            }

        }
    }

    public interface IHttpRequestHelper
    {
        Task<Tuple<HttpStatusCode, string>> MakePostRequest(HttpClient httpClient, string url, string payload, 
            Dictionary<string, string> headers = null);

        Task<Tuple<HttpStatusCode, string>> MakeGetRequest(HttpClient httpClient, string url,
            Dictionary<string, string> headers = null);
    }
}
