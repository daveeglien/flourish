﻿namespace Flourish.Models.Responses.otp
{
    public class ComfirmOtpResponse
    {
        public string UserToken { get; set; }

        public string Otp { get; set; }
    }
}