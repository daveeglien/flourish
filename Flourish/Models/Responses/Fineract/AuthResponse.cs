﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses.Fineract
{
    public class AuthResponse
    {
        public string username { get; set; }
        public string userId { get; set; }
        public string base64EncodedAuthenticationKey { get; set; }
        public bool authenticated { get; set; }
        public string officeId { get; set; }
        public string officeName { get; set; }
        public List<Role> roles { get; set; }
        public List<string> permissions { get; set; }
        public bool shouldRenewPassword { get; set; }
        public bool isTwoFactorAuthenticationRequired { get; set; }
    }

    public class Role
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool disabled { get; set; }
    }


}
