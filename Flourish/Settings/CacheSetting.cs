﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Settings
{
    public class CacheSetting
    {
        public int ExpiryInSeconds { get; set; }
    }
}
