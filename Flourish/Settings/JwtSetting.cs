﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Config
{
    public class JwtSetting
    {
        public string JwtKey { get; set; }
    }
}
