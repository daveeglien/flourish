﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses.Fineract
{

    public class LoansDetailResponse
    {
        public int id { get; set; }
        public string accountNo { get; set; }
        public Status status { get; set; }
        public int clientId { get; set; }
        public string clientName { get; set; }
        public int clientOfficeId { get; set; }
        public int loanProductId { get; set; }
        public string loanProductName { get; set; }
        public string loanProductDescription { get; set; }
        public int loanPurposeId { get; set; }
        public string loanPurposeName { get; set; }
        public int loanOfficerId { get; set; }
        public string loanOfficerName { get; set; }
        public LoanType loanType { get; set; }
        public Currency currency { get; set; }
        public decimal principal { get; set; }
        public int termFrequency { get; set; }
        public TermPeriodFrequencyType termPeriodFrequencyType { get; set; }
        public int numberOfRepayments { get; set; }
        public int repaymentEvery { get; set; }
        public RepaymentFrequencyType repaymentFrequencyType { get; set; }
        public decimal interestRatePerPeriod { get; set; }
        public InterestRateFrequencyType interestRateFrequencyType { get; set; }
        public decimal annualInterestRate { get; set; }
        public AmortizationType amortizationType { get; set; }
        public InterestType interestType { get; set; }
        public InterestCalculationPeriodType interestCalculationPeriodType { get; set; }
        public int transactionProcessingStrategyId { get; set; }
        public Timeline timeline { get; set; }
        public Summary summary { get; set; }
    }
    public class LoanDetailStatus
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
        public bool pendingApproval { get; set; }
        public bool waitingForDisbursal { get; set; }
        public bool active { get; set; }
        public bool closedObligationsMet { get; set; }
        public bool closedWrittenOff { get; set; }
        public bool closedRescheduled { get; set; }
        public bool closed { get; set; }
        public bool overpaid { get; set; }
    }

    public class LoanDetailLoanType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailCurrency
    {
        public string code { get; set; }
        public string name { get; set; }
        public int decimalPlaces { get; set; }
        public string displaySymbol { get; set; }
        public string nameCode { get; set; }
        public string displayLabel { get; set; }
    }

    public class LoanDetailTermPeriodFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailRepaymentFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailInterestRateFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailAmortizationType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailInterestType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailInterestCalculationPeriodType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoanDetailTimeline
    {
        public List<int> submittedOnDate { get; set; }
        public string submittedByUsername { get; set; }
        public string submittedByFirstname { get; set; }
        public string submittedByLastname { get; set; }
        public List<int> approvedOnDate { get; set; }
        public string approvedByUsername { get; set; }
        public string approvedByFirstname { get; set; }
        public string approvedByLastname { get; set; }
        public List<int> expectedDisbursementDate { get; set; }
        public List<int> actualDisbursementDate { get; set; }
        public string disbursedByUsername { get; set; }
        public string disbursedByFirstname { get; set; }
        public string disbursedByLastname { get; set; }
        public List<int> expectedMaturityDate { get; set; }
    }

    public class LoanDetailCurrency2
    {
        public string code { get; set; }
        public string name { get; set; }
        public int decimalPlaces { get; set; }
        public string displaySymbol { get; set; }
        public string nameCode { get; set; }
        public string displayLabel { get; set; }
    }

    public class LoanDetailLinkedAccount
    {
        public int id { get; set; }
        public string accountNo { get; set; }
    }

    public class DisbursementDetail
    {
        public int id { get; set; }
        public List<int> expectedDisbursementDate { get; set; }
        public double principal { get; set; }
        public double approvedPrincipal { get; set; }
    }

    public class Currency3
    {
        public string code { get; set; }
        public string name { get; set; }
        public int decimalPlaces { get; set; }
        public string displaySymbol { get; set; }
        public string nameCode { get; set; }
        public string displayLabel { get; set; }
    }

    public class ChargeTimeType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class ChargeAppliesTo
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class ChargeCalculationType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class ChargePaymentMode
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class FeeFrequency
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class OverdueCharge
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
        public bool penalty { get; set; }
        public Currency3 currency { get; set; }
        public decimal amount { get; set; }
        public ChargeTimeType chargeTimeType { get; set; }
        public ChargeAppliesTo chargeAppliesTo { get; set; }
        public ChargeCalculationType chargeCalculationType { get; set; }
        public ChargePaymentMode chargePaymentMode { get; set; }
        public int feeInterval { get; set; }
        public FeeFrequency feeFrequency { get; set; }
    }

    public class LoanDetailSummary
    {
        public Currency2 currency { get; set; }
        public decimal principalDisbursed { get; set; }
        public decimal principalPaid { get; set; }
        public decimal principalWrittenOff { get; set; }
        public decimal principalOutstanding { get; set; }
        public decimal principalOverdue { get; set; }
        public decimal interestCharged { get; set; }
        public decimal interestPaid { get; set; }
        public decimal interestWaived { get; set; }
        public decimal interestWrittenOff { get; set; }
        public decimal interestOutstanding { get; set; }
        public decimal interestOverdue { get; set; }
        public decimal feeChargesCharged { get; set; }
        public decimal feeChargesDueAtDisbursementCharged { get; set; }
        public decimal feeChargesPaid { get; set; }
        public decimal feeChargesWaived { get; set; }
        public decimal feeChargesWrittenOff { get; set; }
        public decimal feeChargesOutstanding { get; set; }
        public decimal feeChargesOverdue { get; set; }
        public decimal penaltyChargesCharged { get; set; }
        public decimal penaltyChargesPaid { get; set; }
        public decimal penaltyChargesWaived { get; set; }
        public decimal penaltyChargesWrittenOff { get; set; }
        public decimal penaltyChargesOutstanding { get; set; }
        public decimal penaltyChargesOverdue { get; set; }
        public decimal totalExpectedRepayment { get; set; }
        public decimal totalRepayment { get; set; }
        public decimal totalExpectedCostOfLoan { get; set; }
        public decimal totalCostOfLoan { get; set; }
        public decimal totalWaived { get; set; }
        public decimal totalWrittenOff { get; set; }
        public decimal totalOutstanding { get; set; }
        public double totalOverdue { get; set; }
        public List<int> overdueSinceDate { get; set; }
        public LinkedAccount linkedAccount { get; set; }
        public List<DisbursementDetail> disbursementDetails { get; set; }
        public decimal fixedEmiAmount { get; set; }
        public decimal maxOutstandingLoanBalance { get; set; }
        public bool canDisburse { get; set; }
        public List<object> emiAmountVariations { get; set; }
        public bool inArrears { get; set; }
        public bool isNPA { get; set; }
        public List<OverdueCharge> overdueCharges { get; set; }
    }

}
