﻿using Flourish.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Flourish.Services
{
    public class SmsService : ISmsService
    {
        private readonly TwilioCredentials _twilioCredentials;

        public SmsService(IOptions<TwilioCredentials> twilioCredentials)
        {
            _twilioCredentials = twilioCredentials.Value;
        }

        public void Send(string mobileNumber, string text)
        {
            string accountSid = _twilioCredentials.AccountSid;
            string authToken = _twilioCredentials.AuthToken;

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: text,
                from: new Twilio.Types.PhoneNumber(_twilioCredentials.From),
                to: new Twilio.Types.PhoneNumber($"+{mobileNumber}")
            );
        }
    }

    public interface ISmsService
    {
        void Send(string mobileNumber, string text);
    }
}
