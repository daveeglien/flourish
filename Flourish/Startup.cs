﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Flourish.Services;
using Flourish.Utilities;
using Flourish.Config;
using Flourish.Settings;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net.Http;
using Flourish.Middleware.Authentication;

namespace Flourish
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Setup Dependencies
            services.AddMemoryCache();
            services.AddHttpClient(NamedHttpClients.FineractClient, client =>
            {
                client.BaseAddress = new Uri(Configuration.GetValue<string>($"{nameof(FineractEndpoints)}:BaseUrl"));
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) => true
            });

            //Setup System Services
            services.AddTransient<ICacheService, CacheService>();
            services.AddTransient<IFineractService, FineractService>();
            services.AddTransient<IJwtService, JwtService>();
            services.AddTransient<ISmsService, SmsService>();
            services.AddTransient<IOtpService, OtpService>();

            //Setup Utilities
            services.AddTransient<IHttpRequestHelper, HttpRequestHelper>();

            //Bind Configurations
            services.Configure<FineractEndpoints>(Configuration.GetSection($"{nameof(FineractEndpoints)}"));
            services.Configure<FineractCredentials>(Configuration.GetSection($"{nameof(FineractCredentials)}"));
            services.Configure<JwtSetting>(Configuration.GetSection($"{nameof(JwtSetting)}"));
            services.Configure<CacheSetting>(Configuration.GetSection($"{nameof(CacheSetting)}"));
            services.Configure<TwilioCredentials>(Configuration.GetSection($"{nameof(TwilioCredentials)}"));

            services.AddAuthentication().AddBearer(BearerAuthDefaults.BearerScheme,
                    options => {});

            //Add documentation
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Flourish Mobile/Ussd Proxy Api", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My Flourish V1");
            });


        }
    }
}
