﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Flourish.Config;
using Flourish.Models.Responses.Fineract;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Flourish.Middleware.Authentication
{
    public static class AuthenticationBuilderExtensions
    {
        public static AuthenticationBuilder AddBearer(this AuthenticationBuilder builder,
                string authenticationScheme,
                Action<BearerAuthOptions> configureOptions)
        {
            builder.Services.AddSingleton<IPostConfigureOptions<BearerAuthOptions>, BearerAuthPostConfiguration>();
            return builder.AddScheme<BearerAuthOptions, BearerAuthHandler>(authenticationScheme,
                    configureOptions);
        }
    }

    public class BearerAuthHandler : AuthenticationHandler<BearerAuthOptions>
    {
        private const string AuthorizationHeaderName = "Authorization";
        private readonly JwtSetting _jwtSettings;

        public BearerAuthHandler(IOptionsMonitor<BearerAuthOptions> options,
            IOptions<JwtSetting> jwtSettings,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock) : base(options,
            logger,
            encoder,
            clock)
        {
            _jwtSettings = jwtSettings.Value;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(AuthorizationHeaderName)) return Task.FromResult(AuthenticateResult.NoResult());

            if (!AuthenticationHeaderValue.TryParse(Request.Headers[AuthorizationHeaderName],
                out var headerValue)) return Task.FromResult(AuthenticateResult.Fail("No Authorization header provided."));

            if (!Options.Scheme.Contains(headerValue.Scheme)) return Task.FromResult(AuthenticateResult.NoResult());

            var headerValueString = headerValue.Parameter;
            var handler = new JwtSecurityTokenHandler();
            try
            {
                var claimsPrincipal = handler.ValidateToken(headerValueString,
                        new TokenValidationParameters
                        {
                                ValidateIssuer = false,
                                ValidateAudience = false,
                                ValidateLifetime = false,
                                ValidateIssuerSigningKey = true,
                                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.JwtKey))
                        },
                        out SecurityToken validatedToken);
                var serializedUser = new Claim(
                        "client", claimsPrincipal.Claims.First(x => x.Type.ToString() == "client").Value).Value;

                var userProfile = JsonConvert.DeserializeObject<Client>(serializedUser);

                var userAccountNo = new Claim("accountNumber",
                        userProfile.accountNo);

                var user = new Claim("user",
                        JsonConvert.SerializeObject(userProfile));

                var schemeClaim = new Claim(ClaimTypes.AuthenticationMethod,
                        headerValue.Scheme);

                var claims = new[]
                {
                        userAccountNo,
                        user,
                        schemeClaim
                };
                var identity = new ClaimsIdentity(claims,
                        BearerAuthDefaults.BearerScheme);

                //Logger.LogInformation("successfully authenticated basic auth for {0}",
                //    accountProfile.AccountId);

                var ticket = new AuthenticationTicket(new ClaimsPrincipal(identity),
                        headerValue.Scheme);

                return Task.FromResult(AuthenticateResult.Success(ticket));
            }
            catch (Exception)
            {
                return Task.FromResult(AuthenticateResult.NoResult());
            }
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.Headers["WWW-Authenticate"] = $"{BearerAuthDefaults.BearerScheme}, charset=\"UTF-8\"";
            await base.HandleChallengeAsync(properties);
        }
    }
}
