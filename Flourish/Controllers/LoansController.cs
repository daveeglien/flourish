﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flourish.Middleware.Authentication;
using Flourish.Models.Requests.Loans;
using Flourish.Models.Responses;
using Flourish.Models.Responses.Fineract;
using Flourish.Models.Responses.otp;
using Flourish.Services;
using Flourish.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flourish.Controllers
{
    [Authorize(AuthenticationSchemes = BearerAuthOptions.DefaultScheme)]
    public class LoansController : Controller
    {
        private IFineractService _fineractService;


        public LoansController(IFineractService fineractService)
        {
            _fineractService = fineractService;
        }

        [Route("loans")]
        [HttpGet]
        public async Task<IActionResult> Loans()
        {
            var accountNumber = User.FindFirst("accountNumber").Value;
            var userLoans =  await _fineractService.GetLoans(accountNumber);

            var response = new GeneralApiResponse<List<LoanItem>>
            {
                    Status = ApiStatusResponseConstants.Success,
                    Data = userLoans,
                    Message = "User's loans retrieved successfully"
            };

            return Ok(response);
        }

        [Route("loans/{loanId}")]
        [HttpGet]
        public IActionResult LoanDetails([FromRoute] string loanId)
        {
            return Ok();
        }

        [Route("loans/{loanId}/transactions")]
        [HttpGet]
        public IActionResult Transactions([FromRoute] string accountId)
        {
            return Ok();
        }

        [Route("loans/{loanId}/transactions/{transactionId}")]
        [HttpGet]
        public IActionResult TransactionDetail([FromRoute] string accountId, [FromRoute] string transactionId)
        {
            return Ok();
        }

        [Route("requestLoan")]
        [HttpPost]
        public IActionResult RequestLoan([FromBody] RequestLoan model)
        {
            return Ok();
        }

        [Route("loans/{loanId}/payback")]
        [HttpPost]
        public IActionResult PayoffLoan([FromBody] RepayLoan model)
        {
            return Ok();
        }
    }
}
