﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Utilities
{
    public class ApiStatusResponseConstants
    {
        public static string Success = "success";

        public static string Failed = "failed";
    }
}
