﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.google.i18n.phonenumbers;
using Flourish.Models.Responses;
using Flourish.Models.Responses.otp;
using Flourish.Services;
using Flourish.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Tokens;
using static com.google.i18n.phonenumbers.Phonenumber;

namespace Flourish.Controllers
{
    /// <summary>
    /// 1. API to login with Mobile number and receive a PIN to authenticate
    /// 2. API to return user details
    /// 4. API to return Accounts and balances for a user
    /// 5. API to return transaction history filter by date
    /// 6. API to show notifications for a particular user
    /// 7. Api to apply for loan
    /// 8. Api to verify user details
    /// </summary>
    [Route("user")]
    public class UsersController : Controller
    {
        private IFineractService _fineractService;
        private IJwtService _jwtService;
        private ICacheService _cacheService;
        private ISmsService _smsService;
        private IOtpService _otpService;

        public UsersController(IFineractService fineractService, IJwtService jwtService, ICacheService cacheService,
            ISmsService smsService, IOtpService otpService)
        {
            _fineractService = fineractService;
            _jwtService = jwtService;
            _cacheService = cacheService;
            _smsService = smsService;
            _otpService = otpService;
        }

        [Route("{mobileNumber}/register")]
        [HttpGet]
        public async Task<IActionResult> Register(string mobileNumber)
        {

            if (!MobileNumberValidator.IsValidPhoneNumber(mobileNumber))
                return BadRequest("Invalid Mobile Number Provided");

            var userPhoneNumber = MobileNumberValidator.FormatPhoneNumber(mobileNumber);

            var user = await _fineractService.GetUser(userPhoneNumber);
            if (user == null) return BadRequest("No user found with given phone number");

            var jwt = _jwtService.GenerateJwt(user);
            _cacheService.store($"jwt:{userPhoneNumber}", jwt);

            var otp = _otpService.GenerateOtp();
            _cacheService.store($"otp:{userPhoneNumber}", otp);

            _smsService.Send(userPhoneNumber, $"Your flourish  otp is {otp}");

            var response = new GeneralApiResponse<RequestOtpResponse>
            {
                    Status = ApiStatusResponseConstants.Success,
                    Data = new RequestOtpResponse
                    {
                            MobileNumber = userPhoneNumber,
                            OtpPrefix = otp.Split('-')[0]
                    },
                    Message = "Otp generated successfully"
            };

            return Ok(response);

        }

        /// <summary>
        /// Get OTP for mobile app user
        /// </summary>
        /// <param name="mobileNumber"></param>
        /// <returns></returns>
        [Route("{mobileNumber}/login")]
        [HttpGet]
        public async Task<IActionResult> LoginUser(string mobileNumber)
        {

            if (!MobileNumberValidator.IsValidPhoneNumber(mobileNumber))
                return BadRequest("Invalid Mobile Number Provided");

            var userPhoneNumber = MobileNumberValidator.FormatPhoneNumber(mobileNumber);

            var user = await _fineractService.GetUser(userPhoneNumber);
            if(user == null) return BadRequest("No user found with given phone number");

            var jwt = _jwtService.GenerateJwt(user);
            _cacheService.store($"jwt:{userPhoneNumber}", jwt);

            var otp = _otpService.GenerateOtp();
            _cacheService.store($"otp:{userPhoneNumber}", otp);

            _smsService.Send(userPhoneNumber, $"Your flourish  otp is {otp}");

            var response = new GeneralApiResponse<RequestOtpResponse>
            {
                Status = ApiStatusResponseConstants.Success,
                Data = new RequestOtpResponse
                {
                    MobileNumber = userPhoneNumber,
                    OtpPrefix = otp.Split('-')[0]
                },
                Message = "Otp generated successfully"
            };

            return Ok(response);
           
        }

        /// <summary>
        /// Confirm otp for mobile app user
        /// </summary>
        /// <param name="mobileNumber"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        [Route("{mobileNumber}/confirm-otp/{pin}")]
        [HttpGet]
        public IActionResult ConfirmToken([FromRoute]string mobileNumber, [FromRoute] string pin)
        {
            if (!MobileNumberValidator.IsValidPhoneNumber(mobileNumber))
                return BadRequest("Invalid Mobile Number Provided");

            var userPhoneNumber = MobileNumberValidator.FormatPhoneNumber(mobileNumber);

            var retrievedOtp = _cacheService.retrieve($"otp:{userPhoneNumber}");
            if (retrievedOtp == null || !retrievedOtp.ToLower().Equals(pin.ToLower()))
            {
                return BadRequest("Invalid Otp");
            }

            var retrievedJwt = _cacheService.retrieve($"jwt:{userPhoneNumber}");

            var response = new GeneralApiResponse<ComfirmOtpResponse>
            {
                    Status = ApiStatusResponseConstants.Success,
                    Data = new ComfirmOtpResponse
                    {
                            Otp = retrievedOtp,
                            UserToken = retrievedJwt
                    },
                    Message = "OTP confirmed successfully. Use this user's token as the authorisation header (Bearer) for other api requests"
            };

            return Ok(response);
        }
    }
}