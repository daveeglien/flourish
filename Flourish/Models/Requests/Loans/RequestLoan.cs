﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Requests.Loans
{
    public class RequestLoan
    {
        public decimal Amount { get; set; }

        public string Purpose { get; set; }
    }
}
