﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses.Fineract
{

    public class LoansResponse
    {
        public int totalFilteredRecords { get; set; }
        public List<LoanItem> pageItems { get; set; }
    }

    public class LoansStatus
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
        public bool pendingApproval { get; set; }
        public bool waitingForDisbursal { get; set; }
        public bool active { get; set; }
        public bool closedObligationsMet { get; set; }
        public bool closedWrittenOff { get; set; }
        public bool closedRescheduled { get; set; }
        public bool closed { get; set; }
        public bool overpaid { get; set; }
    }

    public class LoanType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class Currency
    {
        public string code { get; set; }
        public string name { get; set; }
        public int decimalPlaces { get; set; }
        public string displaySymbol { get; set; }
        public string nameCode { get; set; }
        public string displayLabel { get; set; }
    }

    public class TermPeriodFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class RepaymentFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class InterestRateFrequencyType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class AmortizationType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class InterestType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class InterestCalculationPeriodType
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class LoansTimeline
    {
        public List<int> submittedOnDate { get; set; }
        public string submittedByUsername { get; set; }
        public string submittedByFirstname { get; set; }
        public string submittedByLastname { get; set; }
        public List<int> approvedOnDate { get; set; }
        public string approvedByUsername { get; set; }
        public string approvedByFirstname { get; set; }
        public string approvedByLastname { get; set; }
        public List<int> expectedDisbursementDate { get; set; }
        public List<int> actualDisbursementDate { get; set; }
        public string disbursedByUsername { get; set; }
        public string disbursedByFirstname { get; set; }
        public string disbursedByLastname { get; set; }
        public List<int> expectedMaturityDate { get; set; }
    }

    public class LinkedAccount
    {
        public int id { get; set; }
        public string accountNo { get; set; }
    }

    public class Currency2
    {
        public string code { get; set; }
        public string name { get; set; }
        public int decimalPlaces { get; set; }
        public string displaySymbol { get; set; }
        public string nameCode { get; set; }
        public string displayLabel { get; set; }
    }

    public class Summary
    {
        public Currency2 currency { get; set; }
        public decimal principalDisbursed { get; set; }
        public decimal principalPaid { get; set; }
        public decimal principalWrittenOff { get; set; }
        public decimal principalOutstanding { get; set; }
        public decimal principalOverdue { get; set; }
        public decimal interestCharged { get; set; }
        public decimal interestPaid { get; set; }
        public decimal interestWaived { get; set; }
        public decimal interestWrittenOff { get; set; }
        public decimal interestOutstanding { get; set; }
        public decimal interestOverdue { get; set; }
        public decimal feeChargesCharged { get; set; }
        public decimal feeChargesDueAtDisbursementCharged { get; set; }
        public decimal feeChargesPaid { get; set; }
        public decimal feeChargesWaived { get; set; }
        public decimal feeChargesWrittenOff { get; set; }
        public decimal feeChargesOutstanding { get; set; }
        public decimal feeChargesOverdue { get; set; }
        public decimal penaltyChargesCharged { get; set; }
        public decimal penaltyChargesPaid { get; set; }
        public decimal penaltyChargesWaived { get; set; }
        public decimal penaltyChargesWrittenOff { get; set; }
        public decimal penaltyChargesOutstanding { get; set; }
        public decimal penaltyChargesOverdue { get; set; }
        public decimal totalExpectedRepayment { get; set; }
        public decimal totalRepayment { get; set; }
        public decimal totalExpectedCostOfLoan { get; set; }
        public decimal totalCostOfLoan { get; set; }
        public decimal totalWaived { get; set; }
        public decimal totalWrittenOff { get; set; }
        public double totalOutstanding { get; set; }
        public double totalOverdue { get; set; }
        public List<int> overdueSinceDate { get; set; }
    }

    public class LoanItem
    {
        public int id { get; set; }
        public string accountNo { get; set; }
        public Status status { get; set; }
        public int clientId { get; set; }
        public string clientAccountNo { get; set; }
        public string clientName { get; set; }
        public int clientOfficeId { get; set; }
        public int loanProductId { get; set; }
        public string loanProductName { get; set; }
        public string loanProductDescription { get; set; }
        public LoanType loanType { get; set; }
        public Currency currency { get; set; }
        public decimal principal { get; set; }
        public int termFrequency { get; set; }
        public TermPeriodFrequencyType termPeriodFrequencyType { get; set; }
        public int numberOfRepayments { get; set; }
        public int repaymentEvery { get; set; }
        public RepaymentFrequencyType repaymentFrequencyType { get; set; }
        public decimal interestRatePerPeriod { get; set; }
        public InterestRateFrequencyType interestRateFrequencyType { get; set; }
        public decimal annualInterestRate { get; set; }
        public AmortizationType amortizationType { get; set; }
        public InterestType interestType { get; set; }
        public InterestCalculationPeriodType interestCalculationPeriodType { get; set; }
        public int transactionProcessingStrategyId { get; set; }
        public Timeline timeline { get; set; }
        public LinkedAccount linkedAccount { get; set; }
        public Summary summary { get; set; }
        public decimal feeChargesAtDisbursementCharged { get; set; }
        public bool inArrears { get; set; }
        public bool isNPA { get; set; }
    }
}
