﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Requests.Loans
{
    public class RepayLoan
    {
        public decimal Amount { get; set; }
    }
}
