﻿using Flourish.Models.Responses.Fineract;
using Flourish.Utilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Flourish.Services
{
    public class FineractService : IFineractService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly FineractCredentials _fineractCredentials;
        private readonly FineractEndpoints _fineractEndpoints;
        private readonly ICacheService _cacheService;
        private readonly ILogger _logger;
        private readonly IHttpRequestHelper _httpRequestHelper;

        public FineractService(IHttpClientFactory httpClientFactory, 
            IOptions<FineractCredentials> fineractCredentials,
            IOptions<FineractEndpoints> fineractEndpoints,
            ICacheService cacheService,
            ILogger<FineractService> logger,
            IHttpRequestHelper httpRequestHelper
            )
        {
            _httpClientFactory = httpClientFactory;
            _fineractCredentials = fineractCredentials.Value;
            _fineractEndpoints = fineractEndpoints.Value;
            _cacheService = cacheService;
            _logger = logger;
            _httpRequestHelper = httpRequestHelper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public async Task<Client> GetUser(string phoneNumber)
        {
            var fineractResponse = await SendToFineractServer(_fineractEndpoints.Clients, HttpRequestType.HttpGet, null);

            if(fineractResponse != null)
            {
                var clientsResponse = JsonConvert.DeserializeObject<ClientsResponse>(fineractResponse);
                var client = clientsResponse.pageItems.Find(x => x.mobileNo == phoneNumber);
                if(client == null)
                {
                    _logger.LogDebug("Given phonenumber not found in fineract result");
                }
                return client;
            }
            _logger.LogDebug("Could not get results from fineract");
            return null;
        }

        public async Task<List<LoanItem>> GetLoans(string accountNumer)
        {
            var fineractResponse = await SendToFineractServer($"{_fineractEndpoints.Loans}", HttpRequestType.HttpGet, null);

            if (fineractResponse != null)
            {
                var loansResponse = JsonConvert.DeserializeObject<LoansResponse>(fineractResponse);
                var clientLoans = loansResponse.pageItems.Where(x => x.clientAccountNo == accountNumer);
                return  clientLoans.ToList();
            }
            _logger.LogDebug("Could not get results from fineract");
            return null;
        }

        public async Task<LoansDetailResponse> GetLoan(string loanId)
        {
            var fineractResponse = await SendToFineractServer($"{_fineractEndpoints.Loans}/{loanId}", HttpRequestType.HttpGet, null);

            if (fineractResponse != null)
            {
                var loanResponse = JsonConvert.DeserializeObject<LoansDetailResponse>(fineractResponse);
                return loanResponse;
            }
            _logger.LogDebug("Could not get results from fineract");
            return null;
        }

        public Task<LoansResponse> GetTransactions(string clientId) { throw new NotImplementedException(); }

        public Task<LoansResponse> GetTransaction(string clientId, string transactionId) { throw new NotImplementedException(); }

        public Task<LoansResponse> GetLoanTransactions(string loanId) { throw new NotImplementedException(); }
        public Task<LoansResponse> GetLoanTransaction(string loanId, string transactionId) { throw new NotImplementedException(); }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<string> Authenticate()
        {
            var httpClient = _httpClientFactory.CreateClient(NamedHttpClients.FineractClient);

            var headers = new Dictionary<string, string>();
            headers.Add("Fineract-Platform-TenantId", _fineractCredentials.Tenant);

            var response = await _httpRequestHelper.MakePostRequest(httpClient,
                $"{_fineractEndpoints.BaseUrl}/{_fineractEndpoints.Auth}?username={_fineractCredentials.Username}" +
                $"&password={_fineractCredentials.Password}", null, headers);

            if (response != null && response.Item1.Equals(HttpStatusCode.OK))
            {
                var authResponse = JsonConvert.DeserializeObject<AuthResponse>(response.Item2);
                _cacheService.store(NamedCacheKeys.FineractAuthCacheKey, $"Basic {authResponse.base64EncodedAuthenticationKey}");
                return $"Basic {authResponse.base64EncodedAuthenticationKey}";
            }

            return null;
        }

        private async Task<string> SendToFineractServer(string endpoint, HttpRequestType requestType, string payload)
        {
            var basicAuthHeader = _cacheService.retrieve(NamedCacheKeys.FineractAuthCacheKey);
            if (basicAuthHeader == null)
            {
                _logger.LogDebug("Basic Auth Header not found in cache");
                basicAuthHeader = await Authenticate();
                if (basicAuthHeader == null)
                {
                    _logger.LogError("Could not authenticate against fineract server");
                    return null;
                }
            }

            var httpClient = _httpClientFactory.CreateClient(NamedHttpClients.FineractClient);
            Tuple<HttpStatusCode, string> response = null;

            if (requestType == HttpRequestType.HttpGet)
            {
                var headers = new Dictionary<string, string>();
                headers.Add("Fineract-Platform-TenantId", _fineractCredentials.Tenant);
                headers.Add("Authorization", basicAuthHeader);
                response = await _httpRequestHelper.MakeGetRequest(httpClient,
                    $"{_fineractEndpoints.BaseUrl}/{endpoint}", headers);
            }
            else if(requestType == HttpRequestType.HttpPost)
            {
                var headers = new Dictionary<string, string>();
                headers.Add("Fineract-Platform-TenantId", _fineractCredentials.Tenant);
                headers.Add("Authorization", basicAuthHeader);
                response = await _httpRequestHelper.MakePostRequest(httpClient, $"{_fineractEndpoints.BaseUrl}/{endpoint}", payload, headers);
            }

            if (response != null && !response.Item1.Equals(HttpStatusCode.Unauthorized) && 
                !response.Item1.Equals(HttpStatusCode.BadRequest) && !response.Item1.Equals(HttpStatusCode.BadGateway))
            {
                return response.Item2;
            }

            if (response != null && response.Item1.Equals(HttpStatusCode.Unauthorized))
            {
                _logger.LogError("Request failed due to authentication." +
                    " Attempting to reauthorise against fineract server");
                basicAuthHeader = await Authenticate();
                if (basicAuthHeader == null)
                {
                    _logger.LogError("Could not authenticate against fineract server");
                    return null;
                }
                _cacheService.store(NamedCacheKeys.FineractAuthCacheKey, basicAuthHeader);
            }

            //retry request
            //todo try to swap with polly http
            if (requestType == HttpRequestType.HttpGet)
            {
                var headers = new Dictionary<string, string>();
                headers.Add("Fineract-Platform-TenantId", _fineractCredentials.Tenant);
                headers.Add("Authorization", basicAuthHeader);
                response = await _httpRequestHelper.MakeGetRequest(httpClient,
                    $"{_fineractEndpoints.BaseUrl}/{_fineractEndpoints.Clients}", headers);
            }
            else if (requestType == HttpRequestType.HttpPost)
            {
                var headers = new Dictionary<string, string>();
                headers.Add("Fineract-Platform-TenantId", _fineractCredentials.Tenant);
                headers.Add("Authorization", basicAuthHeader);
                response = await _httpRequestHelper.MakePostRequest(httpClient,
                    $"{_fineractEndpoints.BaseUrl}/{_fineractEndpoints.Clients}"
                    , payload, headers);
            }

            if (response != null && !response.Item1.Equals(HttpStatusCode.Unauthorized) && 
                !response.Item1.Equals(HttpStatusCode.BadRequest) && !response.Item1.Equals(HttpStatusCode.BadGateway))
            {
                return response.Item2;
            }

            return null;
        }

    }

    public interface IFineractService
    {
        Task<Client> GetUser(string phoneNumber);

        Task<List<LoanItem>> GetLoans(string accountNumber);

        Task<LoansDetailResponse> GetLoan(string loanId);

        Task<LoansResponse> GetTransactions(string clientId);

        Task<LoansResponse> GetTransaction(string clientId, string transactionId);
    }
}
