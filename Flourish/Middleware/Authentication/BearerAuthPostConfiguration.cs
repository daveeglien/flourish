﻿using System;
using Microsoft.Extensions.Options;

namespace Flourish.Middleware.Authentication
{
    public class BearerAuthPostConfiguration : IPostConfigureOptions<BearerAuthOptions>
    {
        public void PostConfigure(string name,
                BearerAuthOptions options)
        {
            if (options.Scheme  == null) throw new InvalidOperationException("Schemes must be provided in options");

            if (!options.Scheme.Contains(BearerAuthDefaults.BearerScheme)) throw new InvalidOperationException("Bearer Schemes must be provided in options");
        }
    }
}