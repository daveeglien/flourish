﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Utilities
{
    public class FineractEndpoints
    {
        
        public string BaseUrl { get; set; }

        public string Clients { get; set; }

        public string Loans { get; set; }

        public string Auth { get; set; }
    }

}
