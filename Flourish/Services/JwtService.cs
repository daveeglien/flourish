﻿using Flourish.Config;
using Flourish.Models.Responses.Fineract;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flourish.Services
{
    public class JwtService : IJwtService
    {
        private readonly JwtSetting _jwtSetting;
        private readonly ILogger _logger;

        public JwtService(
            IOptions<JwtSetting> jwtSetting,
            ILogger<JwtService> logger
            )
        {
            _jwtSetting = jwtSetting.Value;
            _logger = logger;
        }

        public string GenerateJwt(Client client)
        {

            string key = _jwtSetting.JwtKey;

            // Create Security key  using private key above:
            // not that latest version of JWT using Microsoft namespace instead of System
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            // Also note that securityKey length should be >256b
            // so you have to make sure that your private key has a proper length
            var credentials = new SigningCredentials
                              (securityKey, SecurityAlgorithms.HmacSha256Signature);

            //  Finally create a Token
            var header = new JwtHeader(credentials);

            //Some PayLoad that contain information about the  customer
            var payload = new JwtPayload
               {
                   { "client", client}
               };

            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);

            return tokenString;
        }
    }

    public interface IJwtService
    {
        string GenerateJwt(Client client);
    }
}
