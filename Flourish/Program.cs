﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Flourish
{
    public class Program
    {
        //public static void Main(string[] args)
        //{
        //    var webHost = new WebHostBuilder()
        //         .UseKestrel()
        //         .UseContentRoot(Directory.GetCurrentDirectory())
        //         .ConfigureAppConfiguration((hostingContext, config) =>
        //         {
        //             var env = hostingContext.HostingEnvironment;
        //             config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        //                   .AddJsonFile($"appsettings.{env.EnvironmentName}.json",
        //                       optional: true, reloadOnChange: true);
        //             config.AddEnvironmentVariables();
        //         })
        //         .ConfigureLogging((hostingContext, logging) =>
        //         {
        //             logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
        //             logging.AddConsole();
        //             logging.AddDebug();
        //             logging.AddEventSourceLogger();
        //         })
        //         .UseStartup<Startup>()
        //         .Build();
        //    webHost.Run();
        //}

        public static int Main(string[] args)
        {
            //Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration).Enrich.FromLogContext().WriteTo.Console().CreateLogger();
            try
            {
                //Log.Information("Starting web host");
                var host = BuildWebHost(args);
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                //Log.Fatal(ex,
                //        "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                //Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHost BuildWebHost(string[] args)
        {
            var config = new ConfigurationBuilder().AddCommandLine(args).Build();
            return WebHost.CreateDefaultBuilder(args).UseContentRoot(Directory.GetCurrentDirectory()).UseConfiguration(config).UseKestrel().UseIISIntegration().UseStartup<Startup>().Build();
        }
    }
}
