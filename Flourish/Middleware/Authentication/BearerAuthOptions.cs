﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace Flourish.Middleware.Authentication
{
    public class BearerAuthOptions : AuthenticationSchemeOptions
    {

        /// <summary>
        /// 
        /// </summary>
        public const string DefaultScheme = "Bearer";
        public string Scheme => DefaultScheme;
    }
}
