﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Settings
{
    public class TwilioCredentials
    {
        public string AccountSid { get; set; }

        public string AuthToken { get; set; }

        public string From { get; set; }
    }
}
