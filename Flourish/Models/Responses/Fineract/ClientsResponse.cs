﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses.Fineract
{
    public class ClientsResponse
    {
        public int totalFilteredRecords { get; set; }
        public List<Client> pageItems { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class SubStatus
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class Gender
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class ClientType
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class ClientClassification
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class Timeline
    {
        public List<int> submittedOnDate { get; set; }
        public string submittedByUsername { get; set; }
        public string submittedByFirstname { get; set; }
        public string submittedByLastname { get; set; }
        public List<int> activatedOnDate { get; set; }
        public string activatedByUsername { get; set; }
        public string activatedByFirstname { get; set; }
        public string activatedByLastname { get; set; }
    }

    public class LegalForm
    {
        public int id { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

    public class Constitution
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class MainBusinessLine
    {
        public bool active { get; set; }
        public bool mandatory { get; set; }
    }

    public class ClientNonPersonDetails
    {
        public Constitution constitution { get; set; }
        public MainBusinessLine mainBusinessLine { get; set; }
    }

    public class Client
    {
        public int id { get; set; }
        public string accountNo { get; set; }
        public Status status { get; set; }
        public SubStatus subStatus { get; set; }
        public bool active { get; set; }
        public List<int> activationDate { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string displayName { get; set; }
        public string mobileNo { get; set; }
        public Gender gender { get; set; }
        public ClientType clientType { get; set; }
        public ClientClassification clientClassification { get; set; }
        public bool isStaff { get; set; }
        public int officeId { get; set; }
        public string officeName { get; set; }
        public Timeline timeline { get; set; }
        public LegalForm legalForm { get; set; }
        public ClientNonPersonDetails clientNonPersonDetails { get; set; }
    }

}
