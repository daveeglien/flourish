﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Services
{
    public class OtpService : IOtpService
    {
        public string GenerateOtp()
        {
            Random index = new Random();
            char[] alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            char[] numbers = "1234567890".ToCharArray();
            return $"{alphabets[index.Next(0, 25)]}{alphabets[index.Next(0, 25)]}{alphabets[index.Next(0, 25)]}-" +
                $"{numbers[index.Next(0, 9)]}{numbers[index.Next(0, 9)]}{numbers[index.Next(0, 9)]}";
        }
    }

    public interface IOtpService
    {
        string GenerateOtp();
    }
}
