﻿using Flourish.Settings;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Services
{
    public class CacheService : ICacheService
    {
        private IMemoryCache _cache;
        private readonly CacheSetting _cacheSetting;

        public CacheService(IMemoryCache memoryCache, 
            IOptions<CacheSetting> cacheSetting)
        {
            _cache = memoryCache;
            _cacheSetting = cacheSetting.Value;
        }

        public string retrieve(string key)
        {
            string cacheEntry;
            _cache.TryGetValue(key, out cacheEntry);
            return cacheEntry;
        }

        public void store(string key, string value)
        {
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromSeconds(_cacheSetting.ExpiryInSeconds));

            // Save data in cache.
            _cache.Set(key, value, cacheEntryOptions);
        }
    }

    public interface ICacheService
    {

        string retrieve(string key);

        void store(string key, string value);
    }
}
