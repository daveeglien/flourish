﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses.otp
{
    public class RequestOtpResponse
    {
        public string OtpPrefix { get; set; }

        public string MobileNumber { get; set; }
    }
}
