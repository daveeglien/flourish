﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Models.Responses
{
    public class GeneralApiResponse<T> 
    {
        public string Status { get; set; }

        public T Data { get; set; }

        public string Message { get; set; }
    }
}
